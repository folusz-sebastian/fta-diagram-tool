import {Injectable} from '@angular/core';
import {JsonElement} from './model/json-element';

@Injectable({
  providedIn: 'root'
})
export class ProbabilityCountingService {
  private topEventProbability = 0.0;

  constructor() {
  }

  countProbability(treeList: JsonElement[]): number {
    const treeListMap = this.listToMap(treeList);
    console.log(treeListMap);

    console.log('treeList:');
    console.log(treeList);

    treeList.forEach(node => {
      if (node.key.substring(0, 7) === 'AndGate' || node.key.substring(0, 6) === 'OrGate') {
        const oldParent = node.parent;
        node.parent = treeListMap.get(oldParent).parent;
      }
    });

    const treeListWithoutRectangles = treeList.filter(node => node.key.substring(0, 9) !== 'Rectangle');

    const mapWithChildren: Map<string, JsonElement[]> = this.groupBy(treeListWithoutRectangles, node => node.parent);

    console.log('mapWithChildren:');
    console.log(mapWithChildren);

    let numOfGates = 0;

    mapWithChildren.forEach((children: JsonElement[], key: string) => {
      console.log(key, children);
      // rozpatryjemy jaką bramką jest tylko bramka na samej górze
      if (key.substring(0, 7) === 'AndGate' && numOfGates < 1) {
          console.log('AndGate');
        let sumOfProbabilities = 1;

        const gateChildren: JsonElement[] = children;
        for (let i = 0; i < gateChildren.length; i++) {
          if (gateChildren[i].key.substring(0, 6) === 'Circle') {
            sumOfProbabilities *= (gateChildren[i].probability / 100);
            console.log('normal circle probability = ' + gateChildren[i].probability / 100);
          } else {
            sumOfProbabilities *= this.getGateProbability(mapWithChildren, gateChildren[i].key);
          }
        }
        this.topEventProbability = +sumOfProbabilities;
        console.log('sumOfProbabilities = ' + sumOfProbabilities);
      } else if (key.substring(0, 6) === 'OrGate' && numOfGates < 1) {
          console.log('OrGate');
          let sumOfProbabilities = 0;

          const gateChildren: JsonElement[] = children;
          for (let i = 0; i < gateChildren.length; i++) {
            console.log('gateChildren.length = ' + gateChildren.length);
            if (gateChildren[i].key.substring(0, 6) === 'Circle') {
              sumOfProbabilities += (gateChildren[i].probability / 100);
            } else {
              sumOfProbabilities += this.getGateProbability(mapWithChildren, gateChildren[i].key);
            }
          }
        console.log('sumOfProbabilities = ' + sumOfProbabilities);
        this.topEventProbability = +sumOfProbabilities;
      } else {
        console.log('else');
      }

      numOfGates++;
    });

    // for (let key of mapWithChildren.keys()) {
    //   console.log(key + ' = ');
    // }

    // console.log(mapWithChildren);

    console.log('topEventProbability = ' + this.topEventProbability);
    return this.topEventProbability;
  }

  listToMap(treeList: JsonElement[]): Map<string, JsonElement> {
    const map = new Map();
    treeList.forEach(node => map.set(node.key, node));
    return map;
  }
  groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });

    map.delete(undefined);
    return map;
  }

  private getGateProbability (mapWithChildren: Map<string, JsonElement[]>, gateKey: string): number {
    const gateChildren: JsonElement[] = mapWithChildren.get(gateKey);
    let sumOfProbabilities = 0;

    if (gateKey.substring(0, 7) === 'AndGate') {
      sumOfProbabilities++;
    }

      for (let i = 0; i < gateChildren.length; i++) {
        if (gateChildren[i].key.substring(0, 6) === 'Circle') {
          console.log('gateChildren.length = ' + gateChildren.length);
          if (gateKey.substring(0, 6) === 'OrGate') {
            sumOfProbabilities += (gateChildren[i].probability / 100);
            console.log('sumOfProbabilities in normal Or = ' + sumOfProbabilities);
          } else {
            sumOfProbabilities *= (gateChildren[i].probability / 100);
            console.log('sumOfProbabilities in normal And = ' + sumOfProbabilities);
          }
        } else {
          console.log('gateChildren.length = ' + gateChildren.length);
            if (gateKey.substring(0, 6) === 'OrGate') {
              sumOfProbabilities += this.getGateProbability(mapWithChildren, gateChildren[i].key);
              console.log('sumOfProbabilities in recursive Or = ' + sumOfProbabilities);
            } else {
              sumOfProbabilities *= this.getGateProbability(mapWithChildren, gateChildren[i].key);
              console.log('sumOfProbabilities in recursive And = ' + sumOfProbabilities);
            }
        }
      }
    return sumOfProbabilities;
    }
  }
