import * as go from 'gojs';
import {Injectable} from '@angular/core';

const KAPPA = 4 * ((Math.sqrt(2) - 1) / 3);

@Injectable()
export class DesiredShapesService {
  private _CachedArrays = [];

  public defineShapes(): void {
    this.defineAndGate();
    this.defineOrGate();
    this.defineXorGate();
    this.defineNorGate();
    this.defineXnorGate();
    this.defineNandGate();
    this.defineInverter();
    this.defineSquareArrow();
    // this.defineHexagon();
  }

  private defineAndGate() {
    go.Shape.defineFigureGenerator('AndGate', function (shape, w, h) {
      const geo = new go.Geometry();
      const cpOffset = KAPPA * .5;
      const fig = new go.PathFigure(0, 0, true);
      geo.add(fig);

      // The gate body
      fig.add(new go.PathSegment(go.PathSegment.Line, .5 * w, 0));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, w, .5 * h, (.5 + cpOffset) * w, 0,
        w, (.5 - cpOffset) * h));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, .5 * w, h, w, (.5 + cpOffset) * h,
        (.5 + cpOffset) * w, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, h).close());
      geo.spot1 = go.Spot.TopLeft;
      geo.spot2 = new go.Spot(.55, 1);
      return geo;
    });
  }

  private defineNandGate() {
    go.Shape.defineFigureGenerator('NandGate', function (shape, w, h) {
      const geo = new go.Geometry();
      const cpxOffset = KAPPA * .5;
      const cpyOffset = KAPPA * .4;
      const cpOffset = KAPPA * .1;
      const radius = .1;
      const centerx = .9;
      const centery = .5;
      const fig = new go.PathFigure(.8 * w, .5 * h, true);
      geo.add(fig);

      // The gate body
      fig.add(new go.PathSegment(go.PathSegment.Bezier, .4 * w, h, .8 * w, (.5 + cpyOffset) * h,
        (.4 + cpxOffset) * w, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, 0));
      fig.add(new go.PathSegment(go.PathSegment.Line, .4 * w, 0));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, .8 * w, .5 * h, (.4 + cpxOffset) * w, 0,
        .8 * w, (.5 - cpyOffset) * h));
      const fig2 = new go.PathFigure((centerx + radius) * w, centery * h, true);
      geo.add(fig2);
      // Inversion
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery + radius) * h,
          (centerx + radius) * w, (centery + cpOffset) * h,
          (centerx + cpOffset) * w, (centery + radius) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, (centerx - radius) * w, centery * h,
          (centerx - cpOffset) * w, (centery + radius) * h,
          (centerx - radius) * w, (centery + cpOffset) * h
        ));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery - radius) * h,
        (centerx - radius) * w, (centery - cpOffset) * h,
        (centerx - cpOffset) * w, (centery - radius) * h
      ));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, (centerx + radius) * w, (centery) * h,
        (centerx + cpOffset) * w, (centery - radius) * h,
        (centerx + radius) * w, (centery - cpOffset) * h
      ));
      geo.spot1 = new go.Spot(0, .05);
      geo.spot2 = new go.Spot(.55, .95);
      return geo;
    });
  }

  private defineNorGate() {
    go.Shape.defineFigureGenerator('NorGate', function (shape, w, h) {
      const geo = new go.Geometry();
      let radius = .5;
      let cpOffset = KAPPA * radius;
      let centerx = 0;
      let centery = .5;
      const fig = new go.PathFigure(.8 * w, .5 * h, true);
      geo.add(fig);

      // Normal
      fig.add(new go.PathSegment(go.PathSegment.Bezier, 0, h, .7 * w, (centery + cpOffset) * h,
        (centerx + cpOffset) * w, (centery + radius) * h));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, 0, 0, .25 * w, .75 * h,
        .25 * w, .25 * h));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, .8 * w, .5 * h, (centerx + cpOffset) * w, (centery - radius) * h,
        .7 * w, (centery - cpOffset) * h));
      radius = .1;
      cpOffset = KAPPA * .1;
      centerx = .9;
      centery = .5;
      const fig2 = new go.PathFigure((centerx - radius) * w, centery * h, true);
      geo.add(fig2);
      // Inversion
      fig2.add(
        new go.PathSegment(
          go.PathSegment.Bezier, centerx * w, (centery - radius) * h,
          (centerx - radius) * w, (centery - cpOffset) * h,
          (centerx - cpOffset) * w, (centery - radius) * h
        ));
      fig2.add(
        new go.PathSegment(
          go.PathSegment.Bezier, (centerx + radius) * w, centery * h,
          (centerx + cpOffset) * w, (centery - radius) * h,
          (centerx + radius) * w, (centery - cpOffset) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery + radius) * h,
          (centerx + radius) * w, (centery + cpOffset) * h,
          (centerx + cpOffset) * w, (centery + radius) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, (centerx - radius) * w, centery * h,
          (centerx - cpOffset) * w, (centery + radius) * h,
          (centerx - radius) * w, (centery + cpOffset) * h
        ));
      geo.spot1 = new go.Spot(.2, .25);
      geo.spot2 = new go.Spot(.6, .75);
      return geo;
    });
  }

  private defineOrGate() {
    go.Shape.defineFigureGenerator('OrGate', function (shape, w, h) {
      const geo = new go.Geometry();
      const radius = .5;
      const cpOffset = KAPPA * radius;
      const centerx = 0;
      const centery = .5;
      const fig = new go.PathFigure(0, 0, true);
      geo.add(fig);

      fig.add(new go.PathSegment(go.PathSegment.Bezier, w, .5 * h, (centerx + cpOffset + cpOffset) * w, (centery - radius) * h,
        .8 * w, (centery - cpOffset) * h));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, 0, h, .8 * w, (centery + cpOffset) * h,
        (centerx + cpOffset + cpOffset) * w, (centery + radius) * h));
      fig.add(new go.PathSegment(go.PathSegment.Bezier, 0, 0, .25 * w, .75 * h, .25 * w, .25 * h).close());
      geo.spot1 = new go.Spot(.2, .25);
      geo.spot2 = new go.Spot(.75, .75);
      return geo;
    });
  }

  private defineXnorGate() {
    go.Shape.defineFigureGenerator('XnorGate', function (shape, w, h) {
      const geo = new go.Geometry();
      let radius = .5;
      let cpOffset = KAPPA * radius;
      let centerx = .2;
      let centery = .5;
      const fig = new go.PathFigure(.1 * w, 0, false);
      geo.add(fig);

      // Normal
      fig.add(new go.PathSegment(go.PathSegment.Bezier, .1 * w, h, .35 * w, .25 * h, .35 * w, .75 * h));
      const fig2 = new go.PathFigure(.8 * w, .5 * h, true);
      geo.add(fig2);
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, .2 * w, h, .7 * w, (centery + cpOffset) * h,
        (centerx + cpOffset) * w, (centery + radius) * h));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, .2 * w, 0, .45 * w, .75 * h, .45 * w, .25 * h));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, .8 * w, .5 * h, (centerx + cpOffset) * w, (centery - radius) * h,
        .7 * w, (centery - cpOffset) * h));
      radius = .1;
      cpOffset = KAPPA * .1;
      centerx = .9;
      centery = .5;
      const fig3 = new go.PathFigure((centerx - radius) * w, centery * h, true);
      geo.add(fig3);
      // Inversion
      fig3.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery - radius) * h,
          (centerx - radius) * w, (centery - cpOffset) * h,
          (centerx - cpOffset) * w, (centery - radius) * h
        ));
      fig3.add(
        new go.PathSegment(go.PathSegment.Bezier, (centerx + radius) * w, centery * h,
          (centerx + cpOffset) * w, (centery - radius) * h,
          (centerx + radius) * w, (centery - cpOffset) * h
        ));
      fig3.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery + radius) * h,
          (centerx + radius) * w, (centery + cpOffset) * h,
          (centerx + cpOffset) * w, (centery + radius) * h
        ));
      fig3.add(new go.PathSegment(go.PathSegment.Bezier, (centerx - radius) * w, centery * h,
        (centerx - cpOffset) * w, (centery + radius) * h,
        (centerx - radius) * w, (centery + cpOffset) * h
      ));
      geo.spot1 = new go.Spot(.4, .25);
      geo.spot2 = new go.Spot(.65, .75);
      return geo;
    });
  }

  private defineXorGate() {
    go.Shape.defineFigureGenerator('XorGate', function (shape, w, h) {
      const geo = new go.Geometry();
      const radius = .5;
      const cpOffset = KAPPA * radius;
      const centerx = .2;
      const centery = .5;
      const fig = new go.PathFigure(.1 * w, 0, false);
      geo.add(fig);

      fig.add(new go.PathSegment(go.PathSegment.Bezier, .1 * w, h, .35 * w, .25 * h, .35 * w, .75 * h));
      const fig2 = new go.PathFigure(.2 * w, 0, true);
      geo.add(fig2);
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, w, .5 * h, (centerx + cpOffset) * w, (centery - radius) * h,
        .9 * w, (centery - cpOffset) * h));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, .2 * w, h, .9 * w, (centery + cpOffset) * h,
        (centerx + cpOffset) * w, (centery + radius) * h));
      fig2.add(new go.PathSegment(go.PathSegment.Bezier, .2 * w, 0, .45 * w, .75 * h, .45 * w, .25 * h).close());
      geo.spot1 = new go.Spot(.4, .25);
      geo.spot2 = new go.Spot(.8, .75);
      return geo;
    });
  }

  private defineInverter() {
    go.Shape.defineFigureGenerator('Inverter', function (shape, w, h) {
      const geo = new go.Geometry();
      const cpOffset = KAPPA * .1;
      const radius = .1;
      const centerx = .9;
      const centery = .5;
      const fig = new go.PathFigure(.8 * w, .5 * h, true);
      geo.add(fig);

      fig.add(new go.PathSegment(go.PathSegment.Line, 0, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, 0));
      fig.add(new go.PathSegment(go.PathSegment.Line, .8 * w, .5 * h));
      const fig2 = new go.PathFigure((centerx + radius) * w, centery * h, true);
      geo.add(fig2);
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery + radius) * h,
          (centerx + radius) * w, (centery + cpOffset) * h,
          (centerx + cpOffset) * w, (centery + radius) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, (centerx - radius) * w, centery * h,
          (centerx - cpOffset) * w, (centery + radius) * h,
          (centerx - radius) * w, (centery + cpOffset) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, centerx * w, (centery - radius) * h,
          (centerx - radius) * w, (centery - cpOffset) * h,
          (centerx - cpOffset) * w, (centery - radius) * h
        ));
      fig2.add(
        new go.PathSegment(go.PathSegment.Bezier, (centerx + radius) * w, centery * h,
          (centerx + cpOffset) * w, (centery - radius) * h,
          (centerx + radius) * w, (centery - cpOffset) * h
        ));
      geo.spot1 = new go.Spot(0, .25);
      geo.spot2 = new go.Spot(.4, .75);
      return geo;
    });
  }

  private defineSquareArrow() {
    return go.Shape.defineFigureGenerator('SquareArrow', function (shape, w, h) {
      let param1 = shape ? shape.parameter1 : NaN; // pointiness of arrow, lower is more pointy
      if (isNaN(param1)) {
        param1 = .7;
      }

      const geo = new go.Geometry();
      const fig = new go.PathFigure(w, .5 * h, true);
      geo.add(fig);
      fig.add(new go.PathSegment(go.PathSegment.Line, param1 * w, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, h));
      fig.add(new go.PathSegment(go.PathSegment.Line, 0, 0));
      fig.add(new go.PathSegment(go.PathSegment.Line, param1 * w, 0).close());
      geo.spot1 = go.Spot.TopLeft;
      geo.spot2 = new go.Spot(param1, 1);
      return geo;
    });
  }

  // private defineHexagon() {
  //   return go.Shape.defineFigureGenerator('Hexagon', function(shape, w, h) {
  //     const points = this.createPolygon(6);
  //     const geo = new go.Geometry();
  //     const fig = new go.PathFigure(points[0].x * w, points[0].y * h, true);
  //     geo.add(fig);
  //
  //     for (let i = 1; i < 6; i++) {
  //       fig.add(new go.PathSegment(go.PathSegment.Line, points[i].x * w, points[i].y * h));
  //     }
  //     fig.add(new go.PathSegment(go.PathSegment.Line, points[0].x * w, points[0].y * h).close());
  //     this.freeArray(points);
  //     geo.spot1 = new go.Spot(.07, .25);
  //     geo.spot2 = new go.Spot(.93, .75);
  //     return geo;
  //   });
  // }
  //
  // private createPolygon(sides) {
  //   // Point[] points = new Point[sides + 1];
  //   const points = this.tempArray();
  //   const radius = .5;
  //   const center = .5;
  //   const offsetAngle = Math.PI * 1.5;
  //   let angle = 0;
  //
  //   // Loop through each side of the polygon
  //   for (let i = 0; i < sides; i++) {
  //     angle = 2 * Math.PI / sides * i + offsetAngle;
  //     points[i] = new go.Point((center + radius * Math.cos(angle)), (center + radius * Math.sin(angle)));
  //   }
  //
  //   // Add the last line
  //   // points[points.length - 1] = points[0];
  //   points.push(points[0]);
  //   return points;
  // }
  //
  // private tempArray() {
  //   const temp = this._CachedArrays.pop();
  //   if (temp === undefined) { return []; }
  //   return temp;
  // }
}
