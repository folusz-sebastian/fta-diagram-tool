import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  nodeDataArray = [
    { key: 'Circle',      probability: 0, color: 'blue', shape: 'Circle' },
    { key: 'Rectangle',   probability: 0, color: 'blue', shape: 'Rectangle', minSize: '70, 50' },
    { key: 'AndGate',     probability: 0, color: 'indianred', shape: 'AndGate', angle: 270 },
    { key: 'OrGate',      probability: 0, color: 'indianred', shape: 'OrGate', angle: 270 },
  ];
}
