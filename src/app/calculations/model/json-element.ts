export class JsonElement {
  key: string;
  shape: string;
  parent: string;
  probability: number;

  constructor(key: string, shape: string, parent: string, probability: number) {
    this.key = key;
    this.shape = shape;
    this.parent = parent;
    this.probability = probability;
  }

  public toString(): string {
    return 'JsonElement[key = ' + this.key + ', shape = ' + this.shape
      + ', parent = ' + this.parent + ', probability = ' + this.probability + ']';
  }
}
