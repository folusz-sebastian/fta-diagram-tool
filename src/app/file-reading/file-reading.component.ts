import { Component, OnInit } from '@angular/core';
import {FtaToolService} from '../service/ftaTool.service';

@Component({
  selector: 'app-file-reading',
  templateUrl: './file-reading.component.html',
  styleUrls: ['./file-reading.component.scss']
})
export class FileReadingComponent implements OnInit {
  fileReader: FileReader = new FileReader();
  fileContent;

  constructor(public ftaToolService: FtaToolService) { }

  ngOnInit() {
  }

  onChange(fileList: FileList) {
    const file = fileList[0];
    if (file !== undefined) {
      const fileType = file.name.split('.').pop().toLowerCase();
      if (fileType === 'json') {
        this.fileReader.onloadend = (x) => {
          this.fileContent = this.fileReader.result;
          this.ftaToolService.uploadedNewData.next(this.fileContent);
        };
        this.fileReader.readAsText(file);
      }
    }
  }
}
