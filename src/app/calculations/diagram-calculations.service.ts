import { Injectable } from '@angular/core';
import {TreeElement} from './model/tree-element';
import {JsonElement} from './model/json-element';
import {GateCoordinates} from './model/gate-coordinates';

@Injectable({
  providedIn: 'root'
})
export class DiagramCalculationsService {

  originalJsonList: JsonElement[] = [];
  treeList: TreeElement[][] = [];
  treeMinList: TreeElement[][] = [];
  constructor() { }

  public countCutSets(passedDiagramJson) {
    if (this.originalJsonList.length !== 0) {
      this.originalJsonList = [];
    }

    if (this.treeList.length !== 0) {
      this.treeList = [];
    }
    console.log('countCutSets()');
    const diagramJson = JSON.parse(passedDiagramJson);

    console.log('Get diagramJson = ' + diagramJson);

    this.getTreeRootElement(diagramJson);

    while (!this.areAllGatesExpanded()) {
      console.log('In while');
      const gateCoordinates: GateCoordinates = this.findGateCoordinates();
      console.log('Found gate coordinates = ' + gateCoordinates.toString());

      const indexI = gateCoordinates.indexI;
      const indexJ = gateCoordinates.indexJ;

      const parentElement = this.treeList[indexI][indexJ];
      const parentChildren: TreeElement [] = [];
      let parentJsonChildren: JsonElement [] = [];

      parentJsonChildren = this.getChildrenOfParent(parentElement);
      let currentNumOfChildren = parentJsonChildren.length;
      const numOfChildren = parentJsonChildren.length;

      while (currentNumOfChildren > 0) {
        const currentJsonElement = parentJsonChildren[numOfChildren - currentNumOfChildren];
        console.log('Found Element = ' + currentJsonElement.toString());

        parentChildren.push(new TreeElement(currentJsonElement.key, currentJsonElement.shape,
          parentElement, currentJsonElement.probability));

        currentNumOfChildren--;
      }
      this.printTreeElements();
      this.replaceParentNodeInTree(parentElement, indexI, indexJ, parentChildren);

      console.log('length = ' + this.treeList.length);
      this.printTreeElements();
      this.printOriginalJsonListElements();

    }
  }

  private getTreeRootElement(diagramJson: any) {
    let rectangleRoot: TreeElement = null;
    let rootJsonElement: TreeElement = null;

    diagramJson.nodeDataArray.forEach(element => {
      if (element.parent === undefined) {
        rectangleRoot = new TreeElement(element.key, element.shape, null, null);
        rootJsonElement = diagramJson.nodeDataArray.find(x => x.parent
          === rectangleRoot.key);

        const treeElementArray: TreeElement[] = [];
        treeElementArray[0] = new TreeElement(rootJsonElement.key, rootJsonElement.shape, null, null);
        this.treeList.push(treeElementArray);
      }

      const originalJsonLine = new JsonElement(element.key, element.shape, element.parent, null);
      this.originalJsonList.push(originalJsonLine);
    });

    this.originalJsonList = this.originalJsonList.filter(obj => obj.key !== rectangleRoot.key);
    this.originalJsonList = this.originalJsonList.filter(obj => obj.key !== rootJsonElement.key);

    console.log('originalJson: ');
    this.printOriginalJsonListElements();

    this.printTreeElements();
  }

  private findGateCoordinates(): GateCoordinates {
    let indexI: number = null;
    let indexJ: number = null;

    this.treeList.some(treeElementRow => {
      treeElementRow.some(treeElementCol => {

        if (treeElementCol.shape === 'Rectangle') {
          indexI = this.getCurrentIndexI(treeElementRow);
          indexJ = this.getCurrentIndexJ(treeElementRow, treeElementCol);

          const gateElement = this.originalJsonList.find(x => x.parent
            === treeElementCol.key && x.shape !== 'Circle');

          this.treeList[indexI][indexJ]
            = new TreeElement(gateElement.key, gateElement.shape, treeElementCol, null);

          return true;
        } else if (treeElementCol.shape === 'OrGate' || treeElementCol.shape === 'AndGate') {
          indexI = this.getCurrentIndexI(treeElementRow);
          indexJ = this.getCurrentIndexJ(treeElementRow, treeElementCol);

          return true;
        }
      });

      if (indexI !== null && indexJ !== null) {
        return true;
      }
    });

    return new GateCoordinates(indexI, indexJ);
  }

  private getCurrentIndexI(treeElementRow: TreeElement[]) {
    return this.treeList.indexOf(treeElementRow);
  }

  private getCurrentIndexJ(treeElementRow: TreeElement[], treeElementCol: TreeElement) {
    return treeElementRow.indexOf(treeElementCol);
  }

  private replaceParentNodeInTree(parentElement: TreeElement, parentElementIndexI: number,
                              parentElementIndexJ: number, parentChildren: TreeElement[]) {

    if (parentElement.shape === 'OrGate') {
      if (parentChildren.length === 1) {
        this.treeList[parentElementIndexI][parentElementIndexJ] = parentChildren[0];
      } else {
        let parentRow: TreeElement[];

        for (const child of parentChildren) {
          parentRow = this.copyParentRow(parentElementIndexI);
          console.log('child = ' + child.toString());
          console.log('parentRow1 = ' + parentRow.toString());
          console.log('parentElementIndexJ = ' + parentElementIndexJ);
          parentRow[parentElementIndexJ] = child;
          this.treeList.push(parentRow);
          console.log('parentRow2 = ' + parentRow.toString());
          console.log('afterPush = ');
          this.printTreeElements();
        }

        this.treeList.splice(parentElementIndexI, 1);
      }
    } else if (parentElement.shape === 'AndGate') {
        console.log('AndGate');
        if (parentChildren.length === 1) {
          this.treeList[parentElementIndexI][parentElementIndexJ] = parentChildren[0];
        } else {
          for (const child of parentChildren) {
            console.log('child = ' + child.toString());
            console.log('parentElementIndexJ = ' + parentElementIndexJ);

            this.treeList[parentElementIndexI]
              [this.findFreeParentRowJIndex(parentElementIndexI)] = child;
            console.log('afterChange = ');
            this.printTreeElements();
          }

          this.treeList[parentElementIndexI].splice(parentElementIndexJ, 1);
        }
      }
  }

  private areAllGatesExpanded() {
    let areAllGatesExpanded = true;
      this.treeList.some(rowObj => {
        rowObj.some( colObj => {
              if (colObj.shape !== 'Circle') {
                areAllGatesExpanded = false;
                return true;
              }
        });
        return false;
      });

      if (areAllGatesExpanded) {
        console.log('All gates are expanded');
      }
      return areAllGatesExpanded;
  }

  private copyParentRow(parentElementIndexI: number) {
    const parentRow: TreeElement[] = [];

    for (const parentElement of this.treeList[parentElementIndexI]) {
      parentRow.push(parentElement);
    }

    return parentRow;
  }

  private findFreeParentRowJIndex(parentElementIndexI: number) {
    return this.treeList[parentElementIndexI].length;
  }
  private printTreeElements() {
    this.treeList.forEach(treeElementRow => {
      console.log('BANG ROW!');
      treeElementRow.forEach(treeElementCol => {
        console.log('BANG COL!');
        console.log('treeListElement = ' + treeElementCol.toString());
      });
    });
  }

  private getChildrenOfParent(treeElement: TreeElement) {
    return this.originalJsonList.filter(x => x.parent === treeElement.key);
  }

  private printOriginalJsonListElements() {
    this.originalJsonList.forEach(jsonElement => {
      console.log('originalJsonListElement = ' + jsonElement.toString());
    });
  }

  public countMinCutSets() {
    console.log('countMinCutSets');

    if (this.treeMinList.length !== 0) {
      this.treeMinList = [];
    }

    if (this.treeList.length === 1) {
      this.treeMinList.push(this.treeList[0]);
    } else {
      for (let i = 0; i < this.treeList.length; i++) {
        if (!this.checkIfThisRowIsToRemove(i)) {
          console.log('Dodano minimalne cięcie');
          this.treeMinList.push(this.treeList[i]);
        }
      }
    }

    console.log('Postać ostateczna minimalnych cięć: ');
    this.treeMinList.forEach(treeMinRow => {
      console.log('ROW');
      treeMinRow.forEach(treeMinCol => {
        console.log('COL');
        console.log(treeMinCol.toString());
      });
    });
  }

  private checkIfThisRowIsToRemove (indexI: number) {
    let anotherRowContainsThisRow = true;
    const treeListRow: TreeElement[] = this.treeList[indexI];

    treeListRow.forEach(col => {
      console.log('treeListCol = ' + col);
    });

    for (let i = 0; i < this.treeList.length; i++) {
      for (let j = 0; j < this.treeList[i].length; j++) {
        if (i !== indexI) {
          console.log('this.treeList[i][j] = ' + this.treeList[i][j]);
          if (this.checkIfOneSetContainsAnother(treeListRow, i)) {
            console.log('anotherRowContainsThisRow changed to true');
            anotherRowContainsThisRow = true;

            if (treeListRow.length > this.treeList[i].length) {
              return true;
            }
          } else {
            console.log('anotherRowContainsThisRow changed to false');
            anotherRowContainsThisRow = false;
          }
        }
      }
    }

    console.log('anotherRowContainsThisRow returns ' + anotherRowContainsThisRow);
    return anotherRowContainsThisRow;
  }

  private checkIfOneSetContainsAnother(treeListRow: TreeElement[], indexI: number) {
    let contains = true;

    if (treeListRow.length < this.treeList[indexI].length) {
      for (let i = 0; i < treeListRow.length; i++) {
        console.log('treeListRow[i].key = ' + treeListRow[i].key
          + ', this.treeList[indexI][j].key = ' + this.treeList[indexI][i].key);
        if (treeListRow[i].key !== this.treeList[indexI][i].key) {
          contains = false;
        }
      }
    } else {
      for (let i = 0; i < this.treeList[indexI].length; i++) {
        console.log('treeListRow[i].key = ' + treeListRow[i].key
          + ', this.treeList[indexI][j].key = ' + this.treeList[indexI][i].key);
        if (treeListRow[i].key !== this.treeList[indexI][i].key) {
          contains = false;
        }
      }
    }

    return contains;
  }

  public printTreeList() {
    return this.treeList.map(row => row.map(treeElement => {
        if (treeElement.key === 'Circle') {
          return treeElement.key.replace('Circle', 'Zdarzenie1');
        } else {
          return treeElement.key.replace('Circle', 'Zdarzenie');
        }
      }
    ).join(', '));
  }

  public printMinCutSets() {
    return this.treeMinList.map(row => row.map(treeElement => {
        if (treeElement.key === 'Circle') {
          return treeElement.key.replace('Circle', 'Zdarzenie1');
        } else {
          return treeElement.key.replace('Circle', 'Zdarzenie');
        }
      }
    ).join(', '));
  }
}
