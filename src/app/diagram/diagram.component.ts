import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import * as go from 'gojs';
import {DesiredShapesService} from './DesiredShapes.service';
import {DiagramCalculationsService} from '../calculations/diagram-calculations.service';
import {FtaToolService} from '../service/ftaTool.service';
import {NodeAndDataModel} from '../model/NodeAndData.model';
import {ProbabilityCountingService} from '../calculations/probability-counting.service';
import {JsonElement} from '../calculations/model/json-element';

const $ = go.GraphObject.make;
@Component({
  selector: 'app-diagram',
  templateUrl: './diagram.component.html',
  styleUrls: ['./diagram.component.scss']
})
export class DiagramComponent implements OnInit, AfterViewInit {
  public diagram: go.Diagram = null;
  private palette: go.Palette;
  private diagramCalculations: DiagramCalculationsService;
  @Input()
  panelDataModel: [];
  dataModel: [];
  cutSets: string[];
  minCutSets: string[];
  probability = 0;

  constructor(
    desiredShapesService: DesiredShapesService,
    diagramCalculations: DiagramCalculationsService,
    public ftaToolService: FtaToolService,
    public probabilityCountingService: ProbabilityCountingService
  ) {
    desiredShapesService.defineShapes();
    this.diagramCalculations = diagramCalculations;
  }

  ngOnInit() {
    this.ftaToolService.nodeDataChangesCommitted.subscribe((nodeAndData: NodeAndDataModel) => {
      this.diagram.model.startTransaction();
      this.diagram.model.set(nodeAndData.selectedNode.data, 'probability', nodeAndData.nodeData.probability);
      this.diagram.model.commitTransaction();
    });

    this.ftaToolService.uploadedNewData.subscribe(data => {
      this.dataModel = JSON.parse(data);
      this.diagram.model = new go.TreeModel(this.dataModel);
    });
  }

  ngAfterViewInit(): void {
    this.diagram = this.initDiagram();
    this.diagram.nodeTemplate = this.initNodeTemplate();
    this.diagram.linkTemplate = this.initLinkTemplate();
    this.diagram.model = new go.TreeModel();
    this.diagram.addDiagramListener('ChangedSelection', () => {
      const node = this.diagram.selection.first();
      this.ftaToolService.nodeSelected.next(node);
    });

    this.palette = new go.Palette('myPaletteDiv');
    this.palette.maxSelectionCount = 1;
    this.palette.nodeTemplateMap = this.diagram.nodeTemplateMap;
    this.palette.model.nodeDataArray = this.panelDataModel;
  }

  private initDiagram(): go.Diagram {
    const diagram: go.Diagram = new go.Diagram('myDiagramDiv');

    const lightgrayHorizontalLine = $(go.Shape, 'LineH', {stroke: 'lightgray', strokeWidth: 0.5});
    const grayHorizontalLine = $(go.Shape, 'LineH', {stroke: 'gray', strokeWidth: 0.5, interval: 10});
    const lightgrayVerticalLine = $(go.Shape, 'LineV', {stroke: 'lightgray', strokeWidth: 0.5});
    const grayVerticalLine = $(go.Shape, 'LineV', {stroke: 'gray', strokeWidth: 0.5, interval: 10});

    diagram.grid = new go.Panel(go.Panel.Grid);
    diagram.grid.add(lightgrayHorizontalLine);
    diagram.grid.add(grayHorizontalLine);
    diagram.grid.add(lightgrayVerticalLine);
    diagram.grid.add(grayVerticalLine);
    diagram.undoManager.isEnabled = true;

    return diagram;
  }

  private initNodeTemplate(): go.Node {
    const node: go.Node = new go.Node(go.Panel.Spot);
    node.locationSpot = go.Spot.Center;
    node.minSize = new go.Size(50, 50);
    node.selectable = true;
    node.resizable = true;
    node.resizeObjectName = 'PANEL';
    node.bind(
      new go.Binding('minSize', 'minSize', go.Size.parse)
    );

    const panel: go.Panel = new go.Panel(go.Panel.Auto);
    const defaultShape: go.Shape = new go.Shape();
    defaultShape.portId = '';
    defaultShape.fromLinkable = true;
    defaultShape.toLinkable = true;
    defaultShape.cursor = 'pointer';

    defaultShape.bind(new go.Binding('figure', 'shape'));
    defaultShape.bind(new go.Binding('fill', 'color'));
    defaultShape.bind(new go.Binding('angle', 'angle'));

    panel.name = 'PANEL';
    panel.add(defaultShape);

    const textBlock = new go.TextBlock();
    textBlock.margin = 2;
    textBlock.maxSize = new go.Size(120, NaN);
    textBlock.wrap = go.TextBlock.WrapFit;
    textBlock.font = 'bold 6pt sans-serif';
    textBlock.stroke = 'white';
    // textBlock.editable = true;
    textBlock.bind(new go.Binding('text', 'key', function (v) {
      if (v === 'Circle') {
        return 'Zdarzenie1';
      } else if (v.substring(0, 6) === 'Circle') {
        return 'Zdarzenie' + v.substring(6, 7);
      } else {
        return '';
      }
    }));

    panel.add(textBlock);

    node.add(panel);
    node.add(this.makePort('T', go.Spot.Top, false, true));
    node.add(this.makePort('L', go.Spot.Left, true, true));
    node.add(this.makePort('R', go.Spot.Right, true, true));
    node.add(this.makePort('B', go.Spot.Bottom, true, false));
    return node;
  }

  private makePort(portId: string, spot, output: boolean, input: boolean) {
    const shapeAsPort = new go.Shape();
    shapeAsPort.fill = '#000';
    shapeAsPort.stroke = null;
    shapeAsPort.cursor = 'pointer';
    shapeAsPort.desiredSize = new go.Size(7, 7);
    shapeAsPort.alignment = spot;
    shapeAsPort.alignmentFocus = spot;
    shapeAsPort.portId = portId;
    shapeAsPort.fromSpot = spot;
    shapeAsPort.toSpot = spot;
    shapeAsPort.fromLinkable = output;
    shapeAsPort.toLinkable = input;
    return shapeAsPort;
  }

  private initLinkTemplate(): go.Link {
    const shapeForLink = new go.Shape();
    const link = new go.Link();
    link.selectable = true;
    link.relinkableFrom = true;
    link.relinkableTo = true;
    link.reshapable = true;
    link.routing = go.Link.AvoidsNodes;
    link.curve = go.Link.JumpOver;
    link.corner = 5;
    link.add(shapeForLink);
    return link;
  }

  private presentCutSets() {
    console.log('presentCutSets()');
    this.diagramCalculations.countCutSets(this.diagram.model.toJson());
    this.cutSets = this.diagramCalculations.printTreeList();
  }

  private presentMinCutSets() {
    console.log('presentMinCutSets()');
    this.diagramCalculations.countCutSets(this.diagram.model.toJson());
    this.diagramCalculations.countMinCutSets();
    this.minCutSets = this.diagramCalculations.printMinCutSets();
  }

  countProbability() {
    const treeList: JsonElement[] = JSON.parse(this.diagram.model.toJson()).nodeDataArray;
    this.probability = this.probabilityCountingService.countProbability(treeList);
  }

  removeModel() {
    this.diagram.model = new go.TreeModel();
  }

  saveAsProject() {
    this.writeContents(JSON.stringify(this.diagram.model.nodeDataArray), 'FTA-model' + '.json', 'text/plain');
  }

  writeContents(content, fileName, contentType) {
    const a = document.createElement('a');
    const file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
  }
}
