# Projekt narzędzia komputerowego wspomagającego analizę drzew błędów (FTA)

### projekt realizowany w zespole 2-osobowym

## 1. Opis problemu i sformułowanie zadania
Zbudować narzędzie umożliwiające projektowanie oraz analizę drzew błędów (FTA). Narzędzie powinno mieć możliwość wyeksportowania oraz zaimportowania projektu z/do pliku. W ramach analizy powinna być możlwiość wyznaczenia następujących charakterystyk:
- wyznaczenie ścieżek krytycznych (ang. _cut sets_)
- wyznaczenie minimalnych ścieżek krytycznych (ang. _min cut sets_)
- wyliczenie prawdopodobieństwa zdarzenia głównego

## 2. Opis narzędzia
Narzędzie posiada graficzny interfejs użytkownika oraz składa się z elementów opisanych na rys. 1.

![tool.JPG](readMeImages/tool.JPG)

rys. 1. Graficzny interfejs użytkownika z opisem głównych elementów

## 3. Funkcjonalność narzędzia
- Z przybornika możemy przeciągać następujące elementy do obszaru roboczego: zdarzenie elementarne (kółko), zdarzenie główne (prostokąt), bramka OR oraz bramka AND.
- Przeciągnięte elementy możemy łączyć ze sobą na obszarze roboczym za pomocą linii uwzględniając jedną ważną zasadę: należy elementy łączyć od elementu nadrzędnego do podrzędnego. Dla przykładu linia łącząca bramkę OR ze zdarzeniem elementarnym została “rozpoczęta” w bramce OR i została zakończona w zdarzeniu elementarnym.
- Przyciski służące do analizy/modyfikacji drzewa prezentują wybrane charakterystyki drzewa.

## 4. Uruchomienie aplikacji (How to start app?)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.4.

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## 5. Podręcznik użytkownika

### Wyznaczenie ścieżek krytycznych (cut sets)

W celu wyznaczenia ścieżek krytycznych należy kliknąd przycisk “Wyznacz ścieżki krytyczne”. Po kliknięciu przycisku, wyznaczone ścieżki krytyczne zostaną zaprezentowane w sposób przedstawiony na rys. 2.

![tool.JPG](readMeImages/cut_sets.JPG)

rys. 2. Wynik programu przedstawiający wyznaczone ścieżki krytyczne

### Wyznaczenie minimalnych ścieżek krytycznych (min cut sets)

W celu wyznaczenia minimalnych ścieżek krytycznych postępujemy analogicznie jak w przypadku wyznaczenia ścieżek krytycznych. Po kliknięciu przycisku, wyznaczone minimalne ścieżki krytyczne zostaną zaprezentowane w sposób przedstawiony na rys. 3.

![tool.JPG](readMeImages/min_cut_sets.JPG)

rys. 3. Wynik programu przedstawiający wyznaczone minimalne ścieżki krytyczne

### Wyczyszczenie obszaru roboczego

Przycisk umożliwia jak sama nazwa wskazuje wyczyszczenie obszaru roboczego (usunięcie dotychczas tworzonego drzewa).

### Wyliczenie prawdopodobieństwa zdarzenia głównego

Prawdopodobieostwo wystąpienia zdarzenia głównego można obliczyć, po uprzednim wprowadzeniu prawdopodobieństw wszystkich zdarzeń elementarnych na niego się składających. W celu wprowadzenia
prawdopodobieostwa zdarzenia elementarnego należy:
1. Kliknąć na zdarzenie elementarne.
2. Wprowadzić prawdopodobieństwo p (0 <= p <= 1) z dokładnością do części setnych.
3. Zatwierdzić wprowadzoną wartość.

Na rys. 4 pokazane jest okienko wprowadzania prawdopodobieństw dla poszczególnych zdarzeń elementarnych. Natomiast na rys. 5 widać wynik programu wyliczającego prawdopodobieństwo wystąpienia zdarzenia głównego.

![tool.JPG](readMeImages/event_probability.JPG)

rys. 4. Okienko wprowadzania prawdopodobieństw dla poszczególnych zdarzeń elementarnych

![tool.JPG](readMeImages/main_probability.JPG)

rys. 5. Wynik programu wyliczającego prawdopodobieństwo wystąpienia zdarzenia głównego

### eksport/import drzewa

Do wykonania importu modelu drzewa FTA służy przycisk „wybierz plik z modelem”. Należy wybrać plik z rozszerzeniem .json, który będzie zawierał poprawnie utworzone drzewo FTA. Do wykonania eksportu modelu drzewa FTA służy przycisk „zapisz”, który zapisuje model drzewa FTA w pliku z rozszerzeniem .json w domyślnej lokalizacji zapisywania plików.