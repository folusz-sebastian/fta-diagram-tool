import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DiagramComponent } from './diagram/diagram.component';
import {DesiredShapesService} from './diagram/DesiredShapes.service';
import { InspectorComponent } from './inspector/inspector.component';
import {FormsModule} from '@angular/forms';
import {FtaToolService} from './service/ftaTool.service';
import {CurrencyMaskModule} from 'ng2-currency-mask';
import { FileReadingComponent } from './file-reading/file-reading.component';

@NgModule({
  declarations: [
    AppComponent,
    DiagramComponent,
    InspectorComponent,
    FileReadingComponent
  ],
    imports: [
        BrowserModule,
        FormsModule,
        CurrencyMaskModule
    ],
  providers: [DesiredShapesService, FtaToolService],
  bootstrap: [AppComponent]
})
export class AppModule { }
