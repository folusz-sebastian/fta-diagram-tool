export class GateCoordinates {
  indexI: number;
  indexJ: number;

  constructor(indexI: number,  indexJ: number) {
    this.indexI = indexI;
    this.indexJ = indexJ;
  }

  public toString(): string {
    return 'GateCoordinates[indexI = ' + this.indexI + ', indexJ = ' + this.indexJ + ']';
  }
}
