import {NodeDataModel} from './nodeData.model';

export class NodeAndDataModel {
  public selectedNode: go.Part;
  public nodeData: NodeDataModel;
}
