import {Subject} from 'rxjs';
import {NodeAndDataModel} from '../model/NodeAndData.model';

export class FtaToolService {
  public nodeSelected = new Subject<go.Part>();
  public nodeDataChangesCommitted = new Subject<NodeAndDataModel>();
  public uploadedNewData = new Subject<string>();
}
