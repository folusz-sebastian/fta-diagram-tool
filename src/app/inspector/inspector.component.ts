import {Component, OnInit} from '@angular/core';
import {NodeDataModel} from '../model/nodeData.model';
import {FtaToolService} from '../service/ftaTool.service';
import {NodeAndDataModel} from '../model/NodeAndData.model';

@Component({
  selector: 'app-inspector',
  templateUrl: './inspector.component.html',
  styleUrls: ['./inspector.component.scss']
})
export class InspectorComponent implements OnInit {
  formData: NodeDataModel = new NodeDataModel();
  nodeSelected = false;
  selectedNode: go.Part;
  isCircle: boolean;

  constructor(public ftaToolService: FtaToolService) { }

  ngOnInit() {
    this.ftaToolService.nodeSelected.subscribe((node) => {
      if (node == null) {
        this.nodeSelected = false;
        this.isCircle = false;
      } else if ( node.data.key === undefined) {
        this.nodeSelected = false;
        this.isCircle = false;
      } else {
        this.selectedNode = node;
        this.nodeSelected = true;
        this.formData.probability = node.data.probability;
        this.isCircle = node.data.key.substring(0, 6) === 'Circle';
      }
    });
  }

  onSubmitForm() {
    const nodeAndData = new NodeAndDataModel();
    nodeAndData.selectedNode = this.selectedNode;
    this.formData.probability = +this.formData.probability;
    nodeAndData.nodeData = this.formData;
    this.ftaToolService.nodeDataChangesCommitted.next(nodeAndData);
  }
}
