export class TreeElement {
  key: string;
  shape: string;
  parent: TreeElement;
  probability: number;

  constructor(key: string, shape: string, parent: TreeElement, probability: number) {
    this.key = key;
    this.shape = shape;
    this.parent = parent;
    this.probability = probability;
  }

  public toString(): string {
    return 'TreeElement[key = ' + this.key + ', shape = ' + this.shape
      + ', parent = ' + this.parent + ', probability = ' + this.probability + ']';
  }
}
