import { TestBed } from '@angular/core/testing';

import { DiagramCalculationsService } from './diagram-calculations.service';

describe('DiagramCalculationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DiagramCalculationsService = TestBed.get(DiagramCalculationsService);
    expect(service).toBeTruthy();
  });
});
