export class NodeDataModel {
  public name: string;
  public probability: number;
  public shape: string;
  public parent: string;
}
